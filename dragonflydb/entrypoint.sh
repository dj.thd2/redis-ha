#!/bin/sh

if [ "$REDIS_FORCE_EPOLL" = "true" ]; then
    EXTRA_OPTIONS="$EXTRA_OPTIONS --force_epoll"
fi

exec dragonfly \
    --logtostderr \
    "--cache_mode=${REDIS_CACHE_MODE-true}" \
    "-dbnum" "${REDIS_DB_NUM-1}" \
    --bind 0.0.0.0 \
    --port "${REDIS_PORT-6379}" \
    --save_schedule "${REDIS_SAVE_SCHEDULE-*:05}" \
    "--maxmemory=${REDIS_MAX_MEMORY-1gb}" \
    "--keys_output_limit=${REDIS_KEYS_OUTPUT_LIMIT-12288}" \
    --dir "${REDIS_DB_DIR-/data}" \
    --dbfilename "${REDIS_DB_FILENAME-dump}" \
    "--conn_io_threads=${REDIS_CONN_IO_THREADS-4}" \
    "--proactor_threads=${REDIS_PROACTOR_THREADS-4}" \
    "--conn_use_incoming_cpu=${REDIS_CONN_USE_INCOMING_CPU-true}" \
    "--hz=${REDIS_HZ-10}" \
    "--tcp_nodelay=${REDIS_TCP_NODELAY-true}" \
    "--requirepass=${REDIS_PASS}" "--masterauth=${REDIS_PASS}" $EXTRA_OPTIONS
