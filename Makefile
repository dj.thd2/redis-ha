init:
	make down
	make build

build:
	docker-compose --compatibility build

up:
	docker-compose --compatibility up --no-build

down:
	docker-compose --compatibility down --remove-orphans

lint:
	make down
	docker-compose run --no-deps --rm --user root watchdog python3 -m pylint --reports=no --exit-zero --rcfile=pylint.cfg /application
