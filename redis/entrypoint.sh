#!/bin/sh

mkdir -p /data

if [ "$REDIS_CONF_FILE" != "" ] && [ -e "$REDIS_CONF_FILE" ]; then
	export REDIS_CONF_FILE="$REDIS_CONF_FILE"
else
	export REDIS_CONF_FILE="/redis.conf"
fi

if [ ! -e /data/redis.conf ]; then
	echo "include $REDIS_CONF_FILE" > /data/redis.conf
	echo 'include /data/ip.conf' >> /data/redis.conf
fi

if [ "$ANNOUNCE_HOSTNAME" != "" ]; then
	echo "replica-announce-ip $ANNOUNCE_HOSTNAME" > /data/ip.conf
else
	echo "replica-announce-ip $(hostname -i)" > /data/ip.conf
fi

REDIS_DB_NUM="${REDIS_DB_NUM-2}"
REDIS_PORT="${REDIS_PORT-6379}"
REDIS_MAX_MEMORY="${REDIS_MAX_MEMORY-512m}"
REDIS_DB_DIR="${REDIS_DB_DIR-/data}"
REDIS_DB_FILENAME="${REDIS_DB_FILENAME-dump.rdb}"
REDIS_HZ="${REDIS_HZ-10}"
REDIS_MIN_REPLICAS_TO_WRITE="${REDIS_MIN_REPLICAS_TO_WRITE-1}"

echo "replica-announce-port ${REDIS_PORT}" >> /data/ip.conf
cp /redis.conf.tpl /redis.conf

for i in REDIS_DB_NUM REDIS_PORT REDIS_MAX_MEMORY REDIS_DB_DIR REDIS_DB_FILENAME REDIS_HZ REDIS_PASS REDIS_MIN_REPLICAS_TO_WRITE; do
	eval VALUE="\$$i"
	sed -i "s#{$i}#$VALUE#g" /redis.conf
done

exec redis-server /data/redis.conf
