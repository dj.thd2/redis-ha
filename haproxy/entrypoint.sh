#!/bin/sh

if [ "$(id -u)" -ne 0 ]; then
	exec haproxy -f /data/haproxy.cfg
else
	mkdir -p /data

	if [ -e /haproxy.cfg ]; then
		cp /haproxy.cfg /data/haproxy.cfg
	fi

	for REDIS_HOST in $REDIS_HOSTS null; do
		if [ "$REDIS_HOST" == "" ]; then
			continue
		fi
		if [ "$REDIS_HOST" == "null" ]; then
			continue
		fi
		echo "  server-template $REDIS_HOST 3 $REDIS_HOST:6379" >> /data/haproxy.cfg
		#echo "  server-template ${REDIS_HOST}_backup 3 $REDIS_HOST:6379 no-check backup" >> /data/haproxy.cfg
	done

	sed -i "s/{{DNS_SERVER}}/${DNS_SERVER-127.0.0.11}/g" /data/haproxy.cfg

	if [ "$REDIS_PASS" != "" ]; then
		sed -i "s/{{TCP_CHECK_PASS}}/tcp-check send AUTH\\\\ $REDIS_PASS\\\\r\\\\n\ntcp-check expect string +OK/g" /data/haproxy.cfg
	else
		sed -i "s/{{TCP_CHECK_PASS}}//g" /data/haproxy.cfg
	fi

	if [ "$HAPROXY_DISABLE_MASTER_CHECK" != "true" ]; then
		sed -i "s/{{TCP_CHECK_MASTER}}/tcp-check send info\\\\ replication\\\\r\\\\n\n  tcp-check expect string role:master\n  tcp-check expect string slave0:ip\n  tcp-check expect rstring state=(online|stable_sync)/g" /data/haproxy.cfg
	else
		sed -i "s/{{TCP_CHECK_MASTER}}//g" /data/haproxy.cfg
	fi

	exec su -s /bin/sh haproxy -c /entrypoint.sh
fi
