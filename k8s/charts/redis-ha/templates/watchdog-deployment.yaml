{{- if .Values.watchdog.enabled }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "redis-ha.fullname" . }}-watchdog
  labels:
    {{- include "redis-ha.labels" . | nindent 4 }}
    service: watchdog
    redis-ha-role: internal
spec:
  {{- if not .Values.watchdog.autoscaling.enabled }}
  replicas: {{ .Values.watchdog.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "redis-ha.selectorLabels" . | nindent 6 }}
      service: watchdog
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  template:
    metadata:
      {{- with .Values.watchdog.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "redis-ha.selectorLabels" . | nindent 8 }}
        service: watchdog
        redis-ha-role: internal
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "redis-ha.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.watchdog.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}-watchdog
          securityContext:
            {{- toYaml .Values.watchdog.securityContext | nindent 12 }}
          image: "{{ .Values.watchdog.image.repository }}:{{ .Values.watchdog.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.watchdog.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 8081
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /info
              port: http
          readinessProbe:
            httpGet:
              path: /info
              port: http
          startupProbe:
            httpGet:
              path: /info
              port: http
          {{- with .Values.watchdog.volumeMounts }}
          volumeMounts:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          resources:
            {{- toYaml .Values.watchdog.resources | nindent 12 }}
          env:
            {{- toYaml .Values.watchdog.env | nindent 12 }}
            - name: REDIS_MASTER
              value: ""
            - name: REDIS_SLAVES
              value: "{{ include "redis-ha.fullname" . }}-redis.{{.Release.Namespace}}.svc.{{.Values.config.CLUSTER_LOCAL_SUFFIX}}"
            - name: PEERS_HOSTS
              value: "{{ include "redis-ha.fullname" . }}-watchdog.{{.Release.Namespace}}.svc.{{.Values.config.CLUSTER_LOCAL_SUFFIX}}"
            - name: DNS_SERVER
              value: "{{.Values.config.DNS_SERVER}}"
            - name: DEBUG_MODE
              value: "{{.Values.config.WATCHDOG_DEBUG_MODE}}"
            - name: SOCKET_TIMEOUT
              value: "{{.Values.config.WATCHDOG_SOCKET_TIMEOUT}}"
            - name: ELECTION_TIMEOUT
              value: "{{.Values.config.WATCHDOG_ELECTION_TIMEOUT}}"
            - name: RESOLVER_TIMEOUT
              value: "{{.Values.config.WATCHDOG_RESOLVER_TIMEOUT}}"
            - name: LAST_SEEN_TIMEOUT
              value: "{{.Values.config.WATCHDOG_LAST_SEEN_TIMEOUT}}"
            - name: PEER_TIMEOUT
              value: "{{.Values.config.WATCHDOG_PEER_TIMEOUT}}"
            - name: SCAN_PEERS_INTERVAL
              value: "{{.Values.config.WATCHDOG_SCAN_PEERS_INTERVAL}}"
            - name: CONSENSUS_INTERVAL
              value: "{{.Values.config.WATCHDOG_CONSENSUS_INTERVAL}}"
            - name: WORKER_INTERVAL_WORK_DONE
              value: "{{.Values.config.WATCHDOG_WORKER_INTERVAL_WORK_DONE}}"
            - name: WORKER_INTERVAL_LEADER
              value: "{{.Values.config.WATCHDOG_WORKER_INTERVAL_LEADER}}"
            - name: WORKER_INTERVAL
              value: "{{.Values.config.WATCHDOG_WORKER_INTERVAL}}"
            - name: RESOLVER_LIFETIME
              value: "{{.Values.config.WATCHDOG_RESOLVER_LIFETIME}}"
            - name: REDIS_PASS
              value: "{{.Values.config.REDIS_PASS}}"
      {{- with .Values.watchdog.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.watchdog.volumes }}
      volumes:
        {{- toYaml . | nindent 8 }}
      {{- end}}
      {{- with .Values.watchdog.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.watchdog.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      terminationGracePeriodSeconds: 10
      topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: topology.kubernetes.io/zone
        whenUnsatisfiable: ScheduleAnyway
        labelSelector:
          matchLabels:
            {{- include "redis-ha.selectorLabels" . | nindent 12 }}
            service: watchdog
      - maxSkew: 1
        topologyKey: kubernetes.io/hostname
        whenUnsatisfiable: ScheduleAnyway
        labelSelector:
          matchLabels:
            {{- include "redis-ha.selectorLabels" . | nindent 12 }}
            service: watchdog
{{- end }}
