import os

from dotenv import load_dotenv

load_dotenv()

def to_bool(val):
    return bool(str(val).lower() not in ['', '0', 'false', 'off', 'none', 'null', 'no'])

# pylint: disable=R0903
class ENV:
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    DEBUG_MODE = to_bool(os.getenv('DEBUG_MODE', 'false'))

    REDIS_MASTER = os.getenv('REDIS_MASTER', '')
    REDIS_SLAVES = os.getenv('REDIS_SLAVES', '')
    PEERS_HOSTS = os.getenv('PEERS_HOSTS', '')
    REDIS_PASS = os.getenv('REDIS_PASS', '')

    SOCKET_TIMEOUT = float(os.getenv('SOCKET_TIMEOUT', '1'))
    ELECTION_TIMEOUT = float(os.getenv('ELECTION_TIMEOUT', '3'))
    RESOLVER_TIMEOUT = float(os.getenv('RESOLVER_TIMEOUT', '0.5'))
    LAST_SEEN_TIMEOUT = float(os.getenv('LAST_SEEN_TIMEOUT', '3'))
    PEER_TIMEOUT = float(os.getenv('PEER_TIMEOUT', '1'))
    SCAN_PEERS_INTERVAL = float(os.getenv('SCAN_PEERS_INTERVAL', '1'))
    CONSENSUS_INTERVAL = float(os.getenv('CONSENSUS_INTERVAL', '1'))
    WORKER_INTERVAL_WORK_DONE = float(os.getenv('WORKER_INTERVAL_WORK_DONE', '4'))
    WORKER_INTERVAL_LEADER = float(os.getenv('WORKER_INTERVAL_LEADER', '0.5'))
    WORKER_INTERVAL = float(os.getenv('WORKER_INTERVAL', '0.2'))
    RESOLVER_LIFETIME = float(os.getenv('RESOLVER_LIFETIME', '1'))

    DNS_SERVER = os.getenv('DNS_SERVER', '127.0.0.11')
