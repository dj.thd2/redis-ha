import time
import socket
import asyncio
import random
import logging
import secrets

from threading import Thread

import redis
import requests
import dns.asyncresolver
from flask import Flask, jsonify, request

from settings import ENV

states = [
    "follower",
    "candidate",
    "leader"
]

global_state = {
    "state": "follower",
    "peers": [],
    "known_peer_addresses": [],
    "current_term": 0,
    "voted_for": None,
    "election_timeout": time.time() + (random.randint(500, 2000) / 1000), #time.time() + ENV.ELECTION_TIMEOUT + (random.randint(0, 100) / 1000),
    "peer_id": secrets.token_hex(nbytes=8),
    "leader_id": None,
    "last_election": 0,
}

resolver = dns.asyncresolver.Resolver()
resolver.nameservers = [ENV.DNS_SERVER]
resolver.timeout = ENV.RESOLVER_TIMEOUT
resolver.lifetime = ENV.RESOLVER_LIFETIME

async def scan_peers_background_task(state):
    while True:
        live_peers = []
        known_peer_addresses = []
        now = time.time()
        for peer in state["peers"]:
            if now - ENV.LAST_SEEN_TIMEOUT > peer["last_seen"]:
                print("removed peer: %s [%s]" % (peer["address"], peer["peer_id"]))
                continue
            known_peer_addresses.append(peer["address"])
            live_peers.append(peer)
        state["peers"] = live_peers
        found_addresses = []
        try:
            socket.setdefaulttimeout(ENV.SOCKET_TIMEOUT)
            coros = [resolver.resolve(host, 'A') for host in list(ENV.PEERS_HOSTS.split(','))]
            all_addresses = await asyncio.gather(*coros, return_exceptions=True)
        except Exception as e:
            logging.exception(e)
            all_addresses = []
        for addresses in all_addresses:
            if not isinstance(addresses, dns.resolver.Answer):
                continue
            for address in addresses:
                address = address.to_text()
                if address not in known_peer_addresses:
                    found_addresses.append(address)
                    known_peer_addresses.append(address)
        for address in found_addresses:
            try:
                peer_info = request_peer_info(address)
                if peer_info is None:
                    continue
                if peer_info["peer_id"] == state["peer_id"]:
                    continue
                found = False
                for peer in state["peers"]:
                    if peer["peer_id"] == peer_info["peer_id"] and peer["address"] == address:
                        found = True
                        break
                if found:
                    continue
                peer_info["address"] = address
                peer_info["last_seen"] = time.time()
                state["peers"].append(peer_info)
            except Exception as e:
                logging.exception(e)
                continue
        state["known_peer_addresses"] = known_peer_addresses
        time.sleep(ENV.SCAN_PEERS_INTERVAL + (random.randint(0, 100) / 1000))

def thread_scan_peers_background_task(state):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(scan_peers_background_task(state))
    loop.close()

def consensus_background_task(state):
    while True:
        if state["state"] != "leader" and state["election_timeout"] is not None and time.time() > state["election_timeout"]:
            # convert to candidate
            state["state"] = "candidate"
            print("state: candidate [%s]" % (state["peer_id"]))
            state["current_term"] += 1
            state["voted_for"] = None
            votes = 1
            total = 1
            for peer in state["peers"]:
                try:
                    vote_result = send_request_vote(peer, {"term": state["current_term"], "peer_id": state["peer_id"]})
                    if vote_result is not None:
                        peer["last_seen"] = time.time()
                        if vote_result["result"] and vote_result["granted"]:
                            votes += 1
                except Exception as e:
                    pass
                total += 1
            if total < 3:
                total = 3
            state["election_timeout"] = time.time() + ENV.ELECTION_TIMEOUT + (random.randint(0, 100) / 1000)
            if votes > (total / 2):
                state["last_election"] = time.time()
                state["leader_id"] = state["peer_id"]
                state["state"] = "leader"
                print("state: leader [%s]" % (state["peer_id"]))
        if state["state"] == "leader":
            known_peers = []
            for peer in state["peers"]:
                known_peers.append(peer["peer_id"])
            total = 1
            votes = 1
            for peer in state["peers"]:
                try:
                    send_result = send_append_entries(peer, {"term": state["current_term"], "leader_id": state["leader_id"], "known_peers": known_peers})
                    if send_result is not None:
                        peer["last_seen"] = time.time()
                        if send_result["result"] is True:
                            votes += 1
                except Exception as e:
                    pass
                total += 1
            if total < 3:
                total = 3
            if votes <= (total / 2):
                state["election_timeout"] = 0
                state["state"] = "follower"
                state["leader_id"] = None
                print("state: follower [%s] (call for election)" % (state["peer_id"]))
        time.sleep(ENV.CONSENSUS_INTERVAL + (random.randint(0, 100) / 1000))

async def worker_background_task(state):
    while True:
        work_done = False
        is_leader = False
        if state["state"] == "leader" and state["election_timeout"] is not None and time.time() > state["election_timeout"]:
            is_leader = True
            try:
                masters = []
                slaves = []
                socket.setdefaulttimeout(ENV.SOCKET_TIMEOUT)
                hosts = list(ENV.REDIS_MASTER.split(',') + ENV.REDIS_SLAVES.split(','))
                coros = [resolver.resolve(host, 'A') for host in hosts]
                all_addresses = await asyncio.gather(*coros, return_exceptions=True)
                for host, addresses in zip(hosts, all_addresses):
                    if not isinstance(addresses, dns.resolver.Answer):
                        continue
                    for address in addresses:
                        address = address.to_text()
                        r = None
                        try:
                            socket.setdefaulttimeout(ENV.SOCKET_TIMEOUT)
                            r = redis.Redis(host=address, port=6379, db=0, socket_timeout=ENV.SOCKET_TIMEOUT, password=ENV.REDIS_PASS)
                            info = r.info()
                        except Exception as e:
                            logging.exception(e)
                            continue
                        finally:
                            if r is not None:
                                try:
                                    r.close()
                                except:
                                    pass
                        role = info.get('role', None)
                        master_replid = info.get('master_replid', None)
                        db0_info = info.get('db0', {})
                        if role == 'master':
                            connected_slaves = info.get('connected_slaves', 0)
                            slave_info = []
                            if connected_slaves > 0:
                                for i in range(0, connected_slaves):
                                    slave = info.get('slave' + str(i), None)
                                    if slave is not None:
                                        slave['master_replid'] = master_replid
                                        slave_info.append(slave)
                            masters.append({'ip': address, 'master_replid': master_replid, 'slaves': slave_info, 'db0': db0_info})
                        if role == 'slave' or role == 'replica':
                            master_host = info.get('master_host', None)
                            master_link_status = info.get('master_link_status', None)
                            master_last_io_seconds_ago = info.get('master_last_io_seconds_ago', None)
                            master_sync_in_progress = info.get('master_sync_in_progress', None)
                            slave = {
                                'ip': address,
                                'host': host,
                                'master_host': master_host,
                                'master_link_status': master_link_status,
                                'master_last_io_seconds_ago': master_last_io_seconds_ago,
                                'master_sync_in_progress': master_sync_in_progress,
                                'master_replid': master_replid,
                                'db0': db0_info
                            }
                            slaves.append(slave)

                if len(masters) == 0:
                    if len(slaves) == 0:
                        continue
                    max_slave_db0_keys = 0
                    selected_slave = None
                    for slave in slaves:
                        if not slave:
                            continue
                        slave_db0_keys = slave.get('db0', {}).get('keys', 0)
                        if selected_slave is None or slave_db0_keys > max_slave_db0_keys:
                            selected_slave = slave
                            max_slave_db0_keys = slave_db0_keys
                    if not selected_slave:
                        continue
                    slave = selected_slave
                    new_master_address = slave.get('ip', None)
                    try:
                        socket.setdefaulttimeout(ENV.SOCKET_TIMEOUT)
                        r = redis.Redis(host=new_master_address, port=6379, db=0, socket_timeout=ENV.SOCKET_TIMEOUT, password=ENV.REDIS_PASS)
                        print('%s selected as master!' % (new_master_address))
                        r.replicaof('NO', 'ONE')
                        work_done = True
                    except Exception as e:
                        logging.exception(e)
                        continue
                    for slave in slaves:
                        slave_address = slave.get('ip', None)
                        if slave_address == new_master_address:
                            continue
                        r = None
                        try:
                            socket.setdefaulttimeout(ENV.SOCKET_TIMEOUT)
                            r = redis.Redis(host=slave_address, port=6379, db=0, socket_timeout=ENV.SOCKET_TIMEOUT, password=ENV.REDIS_PASS)
                            print('%s now replicaof %s [master was promoted]' % (slave_address, new_master_address))
                            r.replicaof(new_master_address, '6379')
                            work_done = True
                        except Exception as e:
                            logging.exception(e)
                        if r is not None:
                            try:
                                r.close()
                            except:
                                pass

                elif len(masters) > 1:
                    selected_master_address = None
                    selected_master_slaves = 0
                    selected_master_address_by_db0 = None
                    selected_master_db0_keys = 0
                    for master in masters:
                        master_slaves = master.get('slaves', [])
                        master_db0_keys = master.get('db0', {}).get('keys', 0)
                        if selected_master_address is None or len(master_slaves) > selected_master_slaves:
                            selected_master_address = master.get('ip', None)
                            selected_master_slaves = len(master_slaves)
                        if selected_master_address_by_db0 is None or master_db0_keys > selected_master_db0_keys:
                            selected_master_address_by_db0 = master.get('ip', None)
                            selected_master_db0_keys = master_db0_keys
                    if selected_master_address is None:
                        if selected_master_address_by_db0 is None:
                            continue
                        selected_master_address = selected_master_address_by_db0
                    for instance in masters + slaves:
                        instance_address = instance.get('ip', None)
                        if instance_address == selected_master_address:
                            continue
                        r = None
                        try:
                            socket.setdefaulttimeout(ENV.SOCKET_TIMEOUT)
                            r = redis.Redis(host=instance_address, port=6379, db=0, socket_timeout=ENV.SOCKET_TIMEOUT, password=ENV.REDIS_PASS)
                            print('%s now replicaof %s [double master]' % (instance_address, selected_master_address))
                            r.replicaof(selected_master_address, '6379')
                            work_done = True
                        except Exception as e:
                            logging.exception(e)
                        if r is not None:
                            try:
                                r.close()
                            except:
                                pass

                else:
                    master = masters[0]
                    master_address = master.get('ip', None)
                    for slave in slaves:
                        slave_address = slave.get('ip', None)
                        master_link_status = slave.get('master_link_status', None)
                        master_host = slave.get('master_host', None)
                        if master_host != master_address:
                            r = None
                            try:
                                socket.setdefaulttimeout(ENV.SOCKET_TIMEOUT)
                                r = redis.Redis(host=slave_address, port=6379, db=0, socket_timeout=ENV.SOCKET_TIMEOUT, password=ENV.REDIS_PASS)
                                print('%s now replicaof %s [single master]' % (slave_address, master_address))
                                r.replicaof(master_address, '6379')
                                work_done = True
                            except Exception as e:
                                logging.exception(e)
                            if r is not None:
                                try:
                                    r.close()
                                except:
                                    pass

            except Exception as e:
                logging.exception(e)
        if work_done:
            time.sleep(ENV.WORKER_INTERVAL_WORK_DONE)
        elif is_leader:
            time.sleep(ENV.WORKER_INTERVAL_LEADER)
        else:
            time.sleep(ENV.WORKER_INTERVAL)

def thread_worker_background_task(state):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(worker_background_task(state))
    loop.close()

def request_peer_info(address):
    try:
        r = requests.get('http://'+address+':8081/info', timeout=ENV.PEER_TIMEOUT)
        r = r.json()
    except Exception as e:
        logging.exception(e)
        r = None
    return r

def send_request_vote(peer, data):
    try:
        r = requests.post('http://'+peer["address"]+':8081/request_vote', timeout=ENV.PEER_TIMEOUT, json=data)
        r = r.json()
    except Exception as e:
        logging.exception(e)
        r = None
    return r

def send_append_entries(peer, data):
    try:
        r = requests.post('http://'+peer["address"]+':8081/append_entries', timeout=ENV.PEER_TIMEOUT, json=data)
        r = r.json()
    except Exception as e:
        logging.exception(e)
        r = None
    return r

threads = []

if ENV.PEERS_HOSTS != '':
    process = Thread(target=thread_scan_peers_background_task, args=[global_state])
    process.start()
    threads.append(process)

    process = Thread(target=consensus_background_task, args=[global_state])
    process.start()
    threads.append(process)

process = Thread(target=thread_worker_background_task, args=[global_state])
process.start()
threads.append(process)

app = Flask(__name__)

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

@app.route("/info", methods=["GET"])
def get_info():
    return jsonify({
        "peer_id": global_state["peer_id"],
        "state": global_state["state"],
        "current_term": global_state["current_term"],
        "leader_id": global_state["leader_id"],
        "last_election": global_state["last_election"],
        "voted_for": global_state["voted_for"],
        "known_peer_addresses": global_state["known_peer_addresses"],
        "election_timeout": global_state["election_timeout"],
    })
    #return jsonify(global_state)

@app.route("/append_entries", methods=["POST"])
def append_entries():
    data = request.get_json(force=True)
    if data["term"] < global_state["current_term"]:
        return jsonify({"result": False})
    if data["term"] > global_state["current_term"] or data["leader_id"] != global_state["leader_id"]:
        global_state["current_term"] = data["term"]
        global_state["leader_id"] = data["leader_id"]
        global_state["state"] = "follower"
        print("state: follower [%s]->[%s]" % (global_state["peer_id"], global_state["leader_id"]))
    data["known_peers"].append(data["leader_id"])
    for peer_id in data["known_peers"]:
        if peer_id == global_state["peer_id"]:
            continue
        for peer in global_state["peers"]:
            if peer["peer_id"] == peer_id:
                peer["last_seen"] = time.time()
                break
    global_state["election_timeout"] = time.time() + ENV.ELECTION_TIMEOUT + (random.randint(0, 100) / 1000)
    return jsonify({"result": True, "term": global_state["current_term"]})

@app.route("/request_vote", methods=["POST"])
def request_vote():
    data = request.get_json(force=True)
    if data["term"] < global_state["current_term"]:
        return jsonify({"result": False})
    if data["term"] > global_state["current_term"]:
        global_state["current_term"] = data["term"]
        global_state["voted_for"] = data["peer_id"]
        global_state["state"] = "follower"
        print("state: follower [%s]->[%s]" % (global_state["peer_id"], global_state["leader_id"]))
        global_state["election_timeout"] = time.time() + ENV.ELECTION_TIMEOUT + (random.randint(0, 100) / 1000)
    if global_state["voted_for"] is None or global_state["voted_for"] == data["peer_id"]:
        global_state["voted_for"] = data["peer_id"]
        return jsonify({"result": True, "term": global_state["current_term"], "granted": True})
    return jsonify({"result": True, "term": global_state["current_term"], "granted": False})

if __name__ == "__main__":
    if ENV.PEERS_HOSTS != '':
        app.run(host="0.0.0.0", port=8081, debug=False)
    else:
        global_state["state"] = "leader"
        global_state["last_election"] = 0
    for process in threads:
        process.join()
